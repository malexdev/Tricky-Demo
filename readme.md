# Tricky-Demo

This repository is the demo for the Tricky NPM package.

1. Run [`git clone`](http://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#Cloning-an-Existing-Repository) or [download as zip and extract](http://acrl.ala.org/techconnect/?p=3248).
1. Run `npm install` in your project directory.
    - Note: If you want to use a development version of Tricky instead of one available in NPM, you'll need to replace the `node_modules/tricky` folder.
1. Run `npm start` in your project directory.
1. Open your browser and navigate to `localhost:3000`.
1. Play with the demo!

### Bonus Points!

Run `sh debug.sh` to view Tricky's `debug()` calls!
