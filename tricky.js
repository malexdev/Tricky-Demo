// require server dependencies
const debug = require('debug')('tricky-demo:main');
const path = require('path');
const tricky = require('tricky');

// set up express and socket io
const express = require('express');
const app = express();
const server = require('http').createServer(app);

// set up static server
app.use(express.static(path.join(__dirname, 'public')));

// set up tricky
tricky.init(server);

// start the server
server.listen(3000);

// set up a tricky method
tricky.methods({
    add5: function(input, callback) {
        callback(input + 5);
    }
});
